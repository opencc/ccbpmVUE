### 说明

1. 该项目是驰骋工作流中间件vue版本。
2. 驰骋工作流中间件，有.net/java两个语言的版本，请根据自己的需要下载自己的版本 https://gitee.com/opencc
3. 这两个版本数据库结构、开发文档、视频教程、前端代码完全相同。
4. 代码100%的开源，纯净绿色采用GPL开源协议。

### 应用截图

![发起](public/img/图片.png)

![在途](public/img/%E5%9C%A8%E9%80%94.png)


### 其他资源

1. 官方网站：http://ccflow.org
2. 驰骋低代码开发平台：https://ccfast.cc
3. 在线演示：http://demo.ccflow.org
4. 视频教程: http://ccflow.org/ke.htm
5. 在线文档: http://doc.ccbpm.cn


### 演示地址

1.  http://demo.ccflow.org  用户名: admin  密码：123
2.  该演示网站上有h5版本、vue版本，还有其他的系统，请注意查看。

### 关于我们
1. 作者: 济南驰骋信息技术有限公司. 地址:济南.高新区.碧桂园凤凰国际A座F19.
2. 电话: 0531-82374939, 微信: 18660153393.

