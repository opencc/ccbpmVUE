### 说明

1. SDK表单。
2. 创建流程，节点表单方案使用SDK表单
3. URL的设置
3.1 Demo的例子是按照路由模式配置的，URL设置成路径的name值即可
3.2 Demo的例子是按照组件的模式配置，URL的路径改成绝对路径'@/XX/XX.vue (组件模式在我们表单中没有展示，需要客户根据返回结果值设置)
4. vue表单的页面设置(具体的页面使用可以查看QingJia.vue页面的使用方法)
4.1 工具栏需要使用 tool-bar组件，使用tool-bar组件时，当前页面需要增加一个Save方法，
4.2 如果使用CCFlow系统自带的审核组件，需要引入 WorkCheck组件
4.3 引用组件传的参数在例子中都有描述

