//流程设计模式.
export const FlowDevModel = {
    //专业模式
    Prefessional:0,
    //极简模式（傻瓜表单）
    JiJian: 1,
    //累加模式
    FoolTruck: 2,
    //绑定单表单
    RefOneFrmTree: 3,
    //绑定多表单
    FrmTree: 4,
    //SDK表单
    SDKFrm: 5,
    /// 嵌入式表单
    SelfFrm: 6,
    /// 物联网流程
    InternetOfThings: 7,
    /// 决策树流程
    Tree: 8

}