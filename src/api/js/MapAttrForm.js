import {DBAccess, Entities, Entity} from "../Gener";
import $ from "jquery";
let _this;
function MultipleChoiceSmall(mapExt, mapAttr,obj) {
    _this = obj;
    var data = [];
    switch (parseInt(mapExt.DoWay)) {
        case 1:
            var tag1 = mapExt.Tag1;
            tag1 = tag1.replace(/;/g, ',');
            $.each(tag1.split(","), function (i, o) {
                data.push({No: i, Name: o})
            });
            break;
        case 2:
            var enums = new Entities("BP.Sys.SysEnums");
            enums.Retrieve("EnumKey", mapExt.Tag2);
            if (mapExt.Tag == "1" || mapExt.Tag == "2")
                $.each(enums, function (i, o) {
                    data.push({No: o.EnumKey, Name: o.Lab, IntKey: o.IntKey})
                });
            else
                $.each(enums, function (i, o) {
                    data.push({No: o.IntKey, Name: o.Lab})
                });
            //data = enums;
            break;
        case 3:
            var en = new Entity("BP.Sys.SFTable", mapExt.Tag3);
            data = en.DoMethodReturnJSON("GenerDataOfJson");
            break;
        case 4:
            var tag4SQL = mapExt.Tag4;
            tag4SQL = tag4SQL.replace('@WebUser.No', _this.webUser.No);
            tag4SQL = tag4SQL.replace('@WebUser.Name', _this.webUser.Name);
            tag4SQL = tag4SQL.replace('@WebUser.FK_DeptName', _this.webUser.FK_DeptName);
            tag4SQL = tag4SQL.replace('@WebUser.FK_Dept', _this.webUser.FK_Dept);

            if (tag4SQL.indexOf('@') == 0) {
                alert('约定的变量错误:' + tag4SQL + ", 没有替换下来.");
                return;
            }
            data = DBAccess.RunSQLReturnTable(tag4SQL);
            break;
    }
    return data;
}
export{
    MultipleChoiceSmall, //小范围多选，单选
}
