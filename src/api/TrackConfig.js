//定义样式变量，可在此统一修改流程样式  used
export const STYLE_CURRENT_NODE_COLOR_FORE = 'Red';    //当前节点字体名称颜色
export const STYLE_CURRENT_NODE_BORDER_COLOR = 'red';  //当前节点边框

export const STYLE_CANVAS_COLOR = '#FFFFFF'; //画布背景颜色,E7F1F9
export const STYLE_FONT_SIZE_NODE = 12;  //节点名称字体大小
export const STYLE_FONT_SIZE_LABEL = 12; //标签字体大小

export const STYLE_NODE_WIDTH = 50;  //节点宽度
export const STYLE_NODE_HEIGHT = 50; //节点高度
export const STYLE_NODE_COLOR_FORE = 'Gray';    //节点名称颜色

export const STYLE_NODE_COLOR_FORE_HOVER = 'blue';   //节点名称鼠标悬停时颜色
export const STYLE_NODE_COLOR_FORE_TRACK = 'green';    //走过的节点名称颜色
export const STYLE_NODE_DEFAULT_ICON_PATH = '../../../DataUser/NodeIcon/'; //默认节点图标所在相对路径
export const STYLE_NODE_DEFAULT_ICON = 'Default.jpg';    //默认节点图标文件名称
export const STYLE_NODE_BORDER_RADIUS = 5;   //节点边框圆角大小
export const STYLE_NODE_BORDER_COLOR_FIRST = 'green';    //开始节点边框颜色
export const STYLE_NODE_BORDER_COLOR_END = 'green';    //结束节点边框颜色

export const STYLE_NODE_BORDER_COLOR_TRACK = 'green';  //走过的节点边框颜色
export const STYLE_NODE_BORDER_COLOR = 'black';  //节点边框颜色
export const STYLE_NODE_BORDER_COLOR_HOVER = 'blue'; //节点边框鼠标悬停时颜色

export const STYLE_NODE_BORDER_WIDTH_NORMAL = 1; //节点边框宽度
export const STYLE_NODE_BORDER_WIDTH_HOVER = 3;  //节点边框鼠标悬停时的宽度

export const STYLE_LABEL_COLOR_FORE = 'none';    //标签字体颜色

export const STYLE_LINE_COLOR = 'Gray'; //连线颜色
export const STYLE_LINE_HOVER_COLOR = 'blue';    //连线鼠标悬停时的颜色
export const STYLE_LINE_WIDTH = 1;   //连线宽度
export const STYLE_LINE_TRACK_WIDTH = 2;   //走过的连线宽度
export const STYLE_LINE_TRACK_COLOR = 'green'; //走过的连线颜色
export const STYLE_NODE_TRACK_FONT_SIZE = 14;    //走过的节点轨迹信息字体大小
export const STYLE_NODE_TRACK_FORE_COLOR = 'green';    //走过的节点轨迹信息字体颜色
//export const DATA_USER_ICON_PATH = '/ccflow5/DataUser/UserIcon/';
export const DATA_USER_ICON_PATH = '../../../DataUser/UserIcon/';    //用户头像文件相对目录
export const DATA_USER_ICON_DEFAULT = 'Default.png'; //默认用户头像文件名称
export const DATA_MULTI_USER_ICON_DEFAULT = '../../Img/Multiplayer.png'; //多用户头像文件相对路径